window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
  
    const response = await fetch(url);
  
    if (response.ok) {
      const data = await response.json();
      console.log(data);

      const selectTag = document.getElementById('location');
      for (let location of data.locations) {
        let option = document.createElement('option')
        option.value = location.id
        option.innerHTML = location.name
        selectTag.appendChild(option);
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));


        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation)
        
            /**  const name = document.querySelector("#name").value; 
            const starts = document.querySelector(".starts").value; 
            const ends = document.querySelector(".ends").value; 
            const description = document.querySelector(".description").value; 
            const max_presentations = document.querySelector(".max_presentations").value;
            const max_attendees = document.querySelector(".max_attendees").value;
            const location = document.querySelector(".location").value;
            
            const data = {
                question: question,
                answer: answer,
                value: value,
                categoryId: 1485,
                };
            */
            }
        });
    }
    });
        
    
        
        
        
        
        
        
        

     
        
      
        
        
