window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';
  
    const response = await fetch(url);
  
    if (response.ok) {
      const data = await response.json();
      console.log(data);

      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        let option = document.createElement('option')
        option.value = state.abbreviation
        option.innerHTML = state.name
        selectTag.appendChild(option);
        }

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const name = document.querySelector("#name").value; 
            const starts = document.querySelector(".starts").value; 
            const ends = document.querySelector(".ends").value; 
            const description = document.querySelector(".description").value; 
            const max_presentations = document.querySelector(".max_presentations").value;
            const max_attendees = document.querySelector(".max_attendees").value;
            const location = document.querySelector(".location").value;
            
                
            

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                
            }
        });
    }
  });
        
        
  
        
       
        
        