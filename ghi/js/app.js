function createCard(name, description, pictureUrl,formattedStartDate, formattedEndDate,locationTag) {
    return `
    <div class="card shadow p-0 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top" alt = "">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationTag}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${formattedStartDate} - ${formattedEndDate}
        </div>
    </div>
    
    `;
  }

 

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
        document.body.innerHTML = `<div class="alert alert-danger" role="alert">
        Response error!
      </div>`
        console.error("response error",response)

      } else {
        const data = await response.json();
  

        let colIndex = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const locationTag = details.conference.location.name

                const startDate = new Date(details.conference.starts);
                const endDate = new Date(details.conference.ends);
                
                
                let options = { month: 'numeric', day: '2-digit', year: 'numeric' };
                const formattedStartDate = startDate.toLocaleDateString('en-US', options);
                const formattedEndDate = endDate.toLocaleDateString('en-US', options);
                const html = createCard(name, description, pictureUrl,formattedStartDate, formattedEndDate,locationTag);
                
                const column = document.querySelectorAll('.col');
                let columnIndex = colIndex % 3;
                column[columnIndex].innerHTML += html;
                colIndex++; 
                
            }
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      document.body.innerHTML = `<div class="alert alert-danger" role="alert">
      Fetch error!
    </div>`
      console.error("fetch error",e)
    }
  
  });